﻿using FishhawkLake.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FishhawkLake.Abstract
{
    public interface IDeviceRepository : IRepository<Device>
    {
        IEnumerable<Device> Devices { get; }
    }
}
