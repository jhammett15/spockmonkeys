﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FishhawkLake.Models;

namespace FishhawkLake.Abstract
{
    public interface IAlertRepository : IRepository<Alert>
    {
        IEnumerable<Alert> Alerts { get; }
        IEnumerable<Threshold> Thresholds { get; }

        IEnumerable<Alert> GetAllAlerts();
        int GetCount();

        IEnumerable<Threshold> GetAllThreshholds();
        int GetThreshholds();
    }
}
