﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net.Mail;
using System.Net;
using FishhawkLake.Models;
using FishhawkLake;
using FishhawkLake.Controllers.HelperClasses;
using System.Diagnostics;

namespace FishhawkLake.Controllers.HelperClasses
{
    public class AlertsControllerHelper
    {
        private FishhawkLakeDbContext db;
        public AlertsControllerHelper(FishhawkLakeDbContext SomeDB)
        {
            db = SomeDB;
        }
        public AlertsControllerHelper()
        {
            db = new FishhawkLakeDbContext();
        }



        //Default Alert Result
        private static Boolean textBool = false;
        private static Boolean emailBool = false;
        private static Boolean phoneBool = false;
        //Whether alerts are on/off
        private static Boolean alertBool = true;


        private static String textTri;
        private static String phoneTri;
        private static String ifttt_email = "trigger@applet.ifttt.com";
        private static String cust_email;
        private static String from_email = "alarttestmail123@gmail.com";


        //Assign Value to global variables
        private void assignData()
        {
            try
                {
                    textTri = db.AlertUserContact.Where(v => v.Select == true).Select(v => v.TextTri).First();

                }
                catch (Exception)
                {
                    textTri = "None";

                }
            try
                {
                    phoneTri = db.AlertUserContact.Where(v => v.Select == true).Select(v => v.PhoneTri).First();
                }
                catch (Exception)
                {
                    phoneTri = "None";
                }
            try
                {
                    cust_email = db.AlertUserContact.Where(v => v.Select == true).Select(v => v.Email).First();
                }
                catch (Exception)
                {
                    cust_email = "None";
                }

        }
        public Boolean TextTest()
            {
                return textBool;
            }

        public Boolean PhoneTest()
            {
                return phoneBool;
            }

        public Boolean EmailTest()
            {
                return emailBool;
            }

        // Main alert method
        //Trigger when over threshold 
        public string AlertTrigger(String DID, double val)
            {
                textTri = db.AlertUserContact.Where(v => v.Select == true).Select(v => v.TextTri).First();

                string trigger = "System is performed in safe range.";
                if (AlarmThresholdManager.HasPassedThreshold(DID, val))
                {
                    String text = "Warning!! Device: " + DID + " is over the threshold. Current value: " + val + " Alarm Send!!!";
                    String final = "No Alert Notification Select.";
                    final= AlertTriggerHelper(text,final);

                    addLog(DID, val);

                    Debug.WriteLine("AlertTrigger:" + textBool);

                    trigger = final;

                }
            return trigger;

                
            }

        /********* Helper methods*********/
        private String AlertTriggerHelper(String text, String final) {
            if (alertBool)
            {
                if (emailBool)
                {
                    SendEmail(text);
                    final = text;
                }
                if (textBool)
                {
                    SendText(text);
                    final = text;
                }
                if (phoneBool)
                {
                    SendPhone(text);
                    final = text;
                }
            }
            return final;
        }

        public void addLog(string DID, double val)
            {
                Alert newAlert = new Alert();
                newAlert.Time = DateTime.Now;
                newAlert.Value = 100;
                newAlert.DeviceID = "pubPsi";

                db.Alerts.Add(newAlert);
                db.SaveChanges();
            }

        public void MailHelper(String email, String subject, String message)
            {
                SmtpClient mail = new SmtpClient("smtp.gmail.com");
                mail.Port = 587;
                mail.EnableSsl = true;
                mail.Credentials = new NetworkCredential(from_email, "ZAQ!2wsx");

                MailMessage mailmessage = new MailMessage();
                mailmessage.From = new MailAddress(from_email);
                mailmessage.To.Add(email);
                mailmessage.Subject = subject;
                mailmessage.Body = message;
                mail.Send(mailmessage);
            }

        //Send Text trigger to ifttt
        //text = message info
        private void SendText(String text)
            {
                MailHelper(ifttt_email, textTri, text);
            }

        //Send Email trigger to ifttt
        //text = message info
        private void SendEmail(String text)
            {
                MailHelper(cust_email, "!!!!System Warning!!!!", text);
            }

        //Send Phone Trigger to ifttt
        //text = message info
        private void SendPhone(String text)
            {
                MailHelper(ifttt_email, phoneTri, text);
            }

        public String TestAlert()
            {
                //AlertTrigger("pubPsi", 100);

                return "success";
            }

        public double getVal(string DID)
            {
                return db.Devices.Where(v => v.DeviceID == DID).Select(s => s.Value).ToArray().Last();

            }

       
    }
}