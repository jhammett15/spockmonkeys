namespace FishhawkLake.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AlertUserContact")]
    public partial class AlertUserContact
    {
        [Key]
        public int AID { get; set; }

        [Required]
        [StringLength(128)]
        public string UID { get; set; }

        [Required]
        [StringLength(64)]
        public string PhoneTri { get; set; }

        [Required]
        [StringLength(64)]
        public string TextTri { get; set; }

        [Required]
        [StringLength(64)]
        public string Email { get; set; }

        public bool Select { get; set; }
    }
}
