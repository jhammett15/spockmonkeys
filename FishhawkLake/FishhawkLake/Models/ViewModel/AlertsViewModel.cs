﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FishhawkLake.Models.ViewModel
{
    public class AlertsViewModel
    {
        public AlertSelectionModel alertSelectionModel { set; get; }
        public IEnumerable<Threshold> thresholds { get; set; }
    }
}