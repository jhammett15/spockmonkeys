﻿using FishhawkLake.Abstract;
using FishhawkLake.Concrete;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FishhawkLake.Infrastructure
{
    public class NinjectDependencyResolver
    {
        private IKernel kernel;

        public NinjectDependencyResolver(IKernel kernelParam)
        {
            kernel = kernelParam;
            AddBindings();
        }

        public object GetService(Type serviceType)
        {
            return kernel.TryGet(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return kernel.GetAll(serviceType);
        }

        private void AddBindings()
        {
            kernel.Bind<IAlertRepository>().To<AlertRepository>();
        }
    }
}