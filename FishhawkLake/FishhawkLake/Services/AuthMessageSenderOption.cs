﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FishhawkLake.Services
{
    public class AuthMessageSenderOption
    {
        public string SendGridUser { get; set; }
        public string SendGridKey { get; set; }
    }
}