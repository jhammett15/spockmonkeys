﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json.Linq;
using FishhawkLake.Models;
using FishhawkLake.Controllers;
using FishhawkLake.Controllers.HelperClasses;
using System.Web.Mvc;

namespace FishhawkLakeTests
{
    [TestClass]
    public class YuminTests
    {
        private FishhawkLakeDbContext db = new FishhawkLakeDbContext();

        [TestMethod]
        public void TextTrigTest()
        {
            AlertsController ac = new AlertsController();
            bool text = ac.TextTest();
            Assert.AreEqual(false, text);

        }

        [TestMethod]
        public void PhoneTrigTest()
        {
            AlertsController ac = new AlertsController();
            bool text = ac.PhoneTest();
            Assert.AreEqual(false, text);

        }

        [TestMethod]
        public void EmailTrigTest()
        {
            AlertsController ac = new AlertsController();
            bool text = ac.EmailTest();
            Assert.AreEqual(false, text);

        }

        [TestMethod]
        public void AlertIndexTest()
        {
            AlertsController ac = new AlertsController();
            ActionResult result = ac.Index();
            Assert.IsInstanceOfType(result, typeof(ViewResult));
        }

        [TestMethod]
        public void IFTTT_Test()
        {
            AlertsController ac = new AlertsController();
            string email = ac.getIFTTT();
            Assert.AreEqual(email, "trigger@applet.ifttt.com");
        }

        [TestMethod]
        public void FROM_Test()
        {
            AlertsController ac = new AlertsController();
            string email = ac.getFromEmail();
            Assert.AreEqual(email, "alarttestmail123@gmail.com");
        }

        [TestMethod]
        public void IP_LoopBack_Test()
        {
            PostReqErrorsController prc = new PostReqErrorsController();
            Assert.AreEqual(prc.getLoopBack(), "127.0.0.1");
        }

        [TestMethod]
        public void IPAddress_TypeA_Test()
        {
            PostReqErrorsController prc = new PostReqErrorsController();
            string res = prc.IPRange("10.1.1.1");
            Assert.AreEqual(res, "TypeA");
        }

        [TestMethod]
        public void IPAddress_TypeB_Test()
        {
            PostReqErrorsController prc = new PostReqErrorsController();
            string res = prc.IPRange("172.16.3.1");
            Assert.AreEqual(res, "TypeB");
        }

        [TestMethod]
        public void IPAddress_TypeC_Test()
        {
            PostReqErrorsController prc = new PostReqErrorsController();
            string res = prc.IPRange("192.168.10.2");
            Assert.AreEqual(res, "TypeC");
        }

        [TestMethod]
        public void IPAddress_False_Test()
        {
            PostReqErrorsController prc = new PostReqErrorsController();
            string res = prc.IPRange("1.2.3.4");
            Assert.AreEqual(res, "False");
        }


    }
}
