﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FishhawkLake.Controllers;

namespace FishhawkLakeTests
{
    /// <summary>
    /// Summary description for LucysUnitTests
    /// </summary>
    [TestClass]
    public class LucysTests
    {
        public LucysTests()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void AddOne_True()
        {
            //arrange
            int x = 1;

            //act
            UserController user = new UserController();

            //assert
            var result = user.AddOne(x);
            Assert.AreEqual(x + 1, result);
        }

        [TestMethod]
        public void AddOne_False()
        {
            //arrange
            int x = 1;

            //act
            UserController user = new UserController();

            //assert
            var result = user.AddOne(x);
            Assert.AreNotEqual(x, result);
        }
    }
}
