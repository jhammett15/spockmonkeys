﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json.Linq;
using FishhawkLake.Models;
using FishhawkLake.Controllers.HelperClasses;

namespace FishhawkLakeTests
{
  
    [TestClass]
    public class ApiTests
    {
        JObject goodJObject = new JObject();
        JObject badJObject = new JObject();
        Device goodDevice = new Device();

        [TestInitialize]
        public void Initialize()
        {
            goodJObject.Add("DeviceId", "pubPsi");
            goodJObject.Add("Value", "76.28");
            badJObject.Add("Value", "NotADouble");
            goodDevice.DeviceID = "pubPsi";
            goodDevice.Value = 76.28;
        }


        [TestMethod]
        public void JsonToDatum_JObjectIsNotNull_False_For_Null_Jobject()
        {
            Assert.IsFalse(JsonToDatum.JObjectIsNotNull(null));
        }

        [TestMethod]
        public void JsonToDatum_JObjectIsNotNull_True_For_Valid_Jobject()
        {

            Assert.IsTrue(JsonToDatum.JObjectIsNotNull(goodJObject));
        }

        [TestMethod]
        public void JsonToDatum_ToDatumNoTime_Given_Good_Json_Returns_expected_Device()
        {
            Device testSubject = JsonToDatum.ToDatumNoTime(goodJObject);
            Assert.IsTrue(testSubject.DeviceID.Equals(goodDevice.DeviceID));
            Assert.IsTrue(testSubject.Value.Equals(goodDevice.Value));

        }
        [TestMethod]
        public void JsonToDatum_ToDatumNoTime_Given_Bad_Json_Returns_null_Device_Values()
        {
            Device testSubject = JsonToDatum.ToDatumNoTime(badJObject);
            Assert.IsNull(testSubject.DeviceID);
           // Assert.IsNull(testSubject.Value);
            //Assert.IsTrue(false);
        }


    }
}
