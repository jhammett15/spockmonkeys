# Tools, Configurations, and Packages #

## Visual Studio ##
Microsoft Visual Studio Community 2017 
Version 15.9.11
VisualStudio.15.Release/15.9.6+28307.344
Microsoft .NET Framework
Version 4.7.03056

Installed Version: Community

### Bootstrap ###
v4.3.1

### Entity Framework ###
v6.2.0

### jQuery ###
v3.4.1

### Microsoft.AspNet.Mvc ###
v5.2.7

### Microsoft.AspNet.Razor ###
v3.2.7

### Newtonsoft.Json ###
v12.0.1

### ASP.NET Web Frameworks and Tools ###
5.2.60913.0
ASP.NET Web Frameworks and Tools 2017   

### Microsoft Azure Tools ###
Microsoft Azure Tools   2.9
Microsoft Azure Tools for Microsoft Visual Studio 2017 - v2.9.10730.2

### Microsoft Continuous Delivery Tools for Visual Studio ###
0.4
Simplifying the configuration of Azure DevOps pipelines from within the Visual Studio IDE.

### Sendgrid ###
6.3.4
Make sure you are using this version of Sendgrid instead of the most current one.

### Sendgrid.StmpApi ###
1.3.1
Make sure you are using this version of the package instead of the most current one.

### Selenium Test Instruction ###

Firstly, install selenium extension on you browser. Here is the resources link:
[Selenium IDE](https://www.seleniumhq.org/selenium-ide/)

